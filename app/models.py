from .extensions import db


student_course = db.Table(
    'student_course',
    db.Column('enrollment_id', db.Integer,
              primary_key=True, autoincrement=True),
    db.Column(
        'student_id',
        db.Integer,
        db.ForeignKey('student.student_id'),
        nullable=False
    ),
    db.Column(
        'ecourse_id',
        db.Integer,
        db.ForeignKey('course.course_id'),
        nullable=False
    )
)


class Student(db.Model):
    __tablename__ = 'student'

    student_id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    roll_number = db.Column(db.String(5), nullable=False, unique=True)
    first_name = db.Column(db.String(50), nullable=False)
    last_name = db.Column(db.String(50), nullable=False)
    courses = db.relationship(
        'Course', secondary='student_course', backref='enrollments')


class Course(db.Model):
    __tablename__ = 'course'

    course_id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    course_code = db.Column(db.String(10), nullable=False, unique=True)
    course_name = db.Column(db.String(50), nullable=False)
    course_description = db.Column(db.String(255))
