from flask import Blueprint, render_template, request, redirect, url_for
from ..models import Student, Course
from ..extensions import db

main = Blueprint('main', __name__)


@main.route("/")
def index():
    if request.args:
        # print(request.args['search'])
        filter_string = request.args['search']
        students = Student.query.filter(
            Student.first_name.like('%' + filter_string + '%')
        ).all()
        return render_template(
            "index.html", students=students, filter_string=filter_string)
    students = Student.query.all()
    return render_template("index.html", students=students)


@main.route("/student/<int:id>")
def student_info(id):
    student = Student.query.filter_by(student_id=id).first()
    courses = student.courses
    return render_template(
        "student_details.html", student=student, courses=courses)


@main.route("/student/create", methods=["POST", "GET"])
def add_students():
    courses = Course.query.all()
    if request.method == "POST":
        roll = request.form.get("rollnumber")
        fname = request.form.get("firstname")
        lname = request.form.get("lastname")
        courses = request.form.getlist("courses")
        # print(roll, fname, lname, courses)
        new_student = Student(
            roll_number=roll, first_name=fname, last_name=lname)
        db.session.add(new_student)
        for course in courses:
            # print(Course.query.filter_by(course_id=course).first())
            new_student.courses.append(
                Course.query.filter_by(course_id=course).first())
        db.session.commit()
        return redirect(url_for("main.index"))
    return render_template("form.html", courses=courses)


@main.route("/student/<int:id>/update", methods=["POST", "GET"])
def update_students(id):
    student = Student.query.filter_by(student_id=id).first()
    courses = Course.query.all()
    if request.method == "POST":
        roll = request.form.get("rollnumber")
        fname = request.form.get("firstname")
        lname = request.form.get("lastname")
        courses = request.form.getlist("courses")
        student.roll_number = roll
        student.first_name = fname
        student.last_name = lname
        student.courses.clear()
        for course in courses:
            # print(Course.query.filter_by(course_id=course).first())
            student.courses.append(
                Course.query.filter_by(course_id=course).first())
        db.session.commit()
        return redirect(url_for("main.index"))
    # print(courses)
    return render_template("form.html", student=student, courses=courses)


@main.route("/student/<int:id>/delete", methods=["POST"])
def delete_student(id):
    student = Student.query.filter_by(student_id=id).first()
    db.session.delete(student)
    db.session.commit()
    return redirect(url_for("index"))
