import os
from flask import Flask
from config import config
from .extensions import db
from app.main.routes import main


def create_app(app_config='development'):
    app = Flask(__name__)
    app.config.from_object(config[app_config])
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
    app.config['SQLALCHEMY_DATABASE_URI'] = os.getenv(
        'SQLALCHEMY_DATABASE_URI')
    app.config['SECRET_KEY'] = os.getenv('SECRET_KEY')
    db.init_app(app)
    app.register_blueprint(main)

    return app
